﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="BaiViet.aspx.cs" Inherits="TrialProject.WebForm4" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style5
        {
            width: 70%;
            border:solid 1px silver;
            -moz-border-radius:3px;
            margin-bottom:2px;
        }
        .style6
        {
        }
        .style7
        {
            width: 106px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:DataList ID="DataList3" runat="server" DataSourceID="SqlDataSource2" DataKeyField="newsid">
        <ItemTemplate>
            cateID:
            <asp:Label ID="cateIDLabel" runat="server" Text='<%# Eval("cateID") %>' />
            <br />
            newsid:
            <asp:Label ID="newsidLabel" runat="server" Text='<%# Eval("newsid") %>' />
            <br />
            title:
            <asp:Label ID="titleLabel" runat="server" Text='<%# Eval("title") %>' />
            <br />
            DESCRIPTION:
            <asp:Label ID="DESCRIPTIONLabel" runat="server" Text='<%# Eval("DESCRIPTION") %>' />
            <br />
            content:
            <asp:Label ID="contentLabel" runat="server" Text='<%# Eval("content") %>' />
            <br />
            picture:
            <asp:Label ID="pictureLabel" runat="server" Text='<%# Eval("picture") %>' />
            <br />
            createdate:
            <asp:Label ID="createdateLabel" runat="server" Text='<%# Eval("createdate") %>' />
            <br />
            <br />
        </ItemTemplate>
    </asp:DataList>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:TinTucConnectionString %>" 
        
    
        SelectCommand="SELECT [cateID], [newsid], [title], [DESCRIPTION], [content], [picture], [createdate] FROM [News] WHERE (([cateID] = @cateID) AND ([active] = @active))">
        <SelectParameters>
            <asp:QueryStringParameter Name="cateID" QueryStringField="mid" Type="Int32" />
            <asp:Parameter DefaultValue="1" Name="active" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>
