﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="BaiViet.aspx.cs" Inherits="TrialProject.WebForm4" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style5
        {
            width: 70%;
            border:solid 1px silver;
            -moz-border-radius:3px;
            margin-bottom:2px;
        }
        .style6
        {
        }
        .style7
        {
            width: 106px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:DataList ID="DataList3" runat="server" DataSourceID="SqlDataSource2">
        <ItemTemplate>
            <table class="style5">
                <tr>
                    <td class="style7">
                        <asp:Image ID="Image2" runat="server" Height="90px" 
                            ImageUrl='<%# Eval("picture") %>' Width="90px" />
                    </td>
                    <td>
                        <asp:HyperLink ID="HyperLink1" runat="server" 
                            NavigateUrl='<%# Eval("newsid","noidung.aspx?mbv={0}") %>' 
                            Text='<%# Eval("title") %>'></asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td class="style6" colspan="2">
                        <asp:Label ID="Label8" runat="server" Text='<%# Eval("description") %>'></asp:Label>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:DataList>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
        
    
        SelectCommand="SELECT [cateID], [newsid], [title], [DESCRIPTION], [content], [picture], [createdate] FROM [News] WHERE (([cateID] = @cateID) AND ([active] = @active))">
        <SelectParameters>
            <asp:QueryStringParameter Name="cateID" QueryStringField="mid" Type="Int32" />
            <asp:Parameter DefaultValue="1" Name="active" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>
